package com.wxbus.service;

import com.wxbus.daomain.Passenger;

import java.util.List;

public interface PassengerService {
    /**
     * 查询你所有的乘客信息
     * @return
     */
    List<Passenger> findPassengerList(Integer startNum,Integer num);

    /**
     *
     * @param id
     * @return
     */
    Passenger findPassengerById(Integer id);
    /**
     * 修改乘客的状态信息
     */
    void updatePassengerStatus(Passenger passenger);
    /**
     * 按照乘客身份证号查询乘客是否合法
     */
    boolean isIlleagelPassenger(String passengerCitizenship);

    /**
     *按照乘客身份证号查询主键
     */

    Integer findMainKey(String passengerCitizenship);
}
