package com.wxbus.webController;

import com.wxbus.daomain.*;
import com.wxbus.service.*;
import com.wxbus.util.JacksonUtil;
import com.wxbus.util.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.text.DateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/web/update")
public class WebUpdateController {
    private Log log = LogFactory.getLog(WebUpdateController.class);
    @Autowired
    private RouteService routeService;
    @Autowired
    private PassengerService passengerService;
    @Autowired
    private BusService busService;
    @Autowired
    private DriverBusRouteService driverBusRouteService;
    @Autowired
    private PassengerRouteService passengerRouteService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private DriverService driverService;

    /**
     * @param route
     * @return
     * @author ks
     * @description 发布线路招募时填写的，招募人数，价格等信息
     */
    @PostMapping("/updateroute")
    @ResponseBody
    public Object updateForRecruit(@RequestBody Route route) {
        //添加招募信息
        log.info("添加招募信息");
        Route route1 = routeService.findRouteById(route.getRouteId());
        Integer routStatus = 3;
        route1.setRecruitNum(route.getRecruitNum());
        route1.setPrice(route.getPrice());
        String startRecruit = DateFormat.getDateInstance().format(new Date());
        route1.setRouteStatus(routStatus);
        routeService.updateRoute(route1);

        Message message = new Message();
        message.setSender("管理员");
        message.setMark(0);
        message.setSendtime(new Date());
        message.setReceivetype("乘客");
        message.setTitle("招募公告");
        message.setContent("您申请的线路开始招募了，赶快呼唤好友来助力吧!");
        if (route1.getCreatUser() > 0) {
            message.setReceiver(route1.getCreatUser());
            messageService.insertMessage(message);
        }
        return ResponseUtil.ok();
    }

    /**
     * @param route
     * @return
     * @author ks
     * @description 审核线路时修改的线路的信息
     */
    @PostMapping("/examroute")
    @ResponseBody
    public Object updateForExam(@RequestBody Route route) {
        log.info("审核线路的修改");
        log.info(route.getRouteStatus());
        routeService.updateRoute(route);
        return ResponseUtil.ok();
    }

    /**
     * @param passenger
     * @return
     * @author ks
     * @description 修改乘客的账号状态
     */
    @PostMapping("/passenger")
    @ResponseBody
    public Object updatePassengerStatus(@RequestBody Passenger passenger) {

        Passenger passenger1 = passengerService.findPassengerById(passenger.getId());
        log.info("passenger1" + passenger1.getId());
        if (passenger.getPassengerStatus() == 1) {
            passenger1.setDeleted(passenger1.getDeleted() + 1);
        }
        passenger1.setPassengerStatus(passenger.getPassengerStatus());

        passengerService.updatePassengerStatus(passenger1);
        return ResponseUtil.ok();
    }

    /**
     * @param body
     * @return
     * @author ks
     * @description 分配汽车时 线路状态的修改以及关联表的插入
     */
    @ResponseBody
    @PostMapping("/allotbus")
    public Object allotBus(@RequestBody String body) {


        Integer routeId = JacksonUtil.parseInteger(body, "routeId");
        String busId = JacksonUtil.parseString(body, "busId");
        if(busId==""||routeId==null||busId==null){
            log.info("线路或者汽车不能为空");
        }else{
            DriverBusRoute driverBusRoute = new DriverBusRoute();
            //更改线路状态为运行中
            Route route = routeService.findRouteById(routeId);
            //查询是否有司机，如果有就改变线路状态 为开通并且 发送消息
            DriverBusRoute driverBusRoute1 = driverBusRouteService.findInfoByRouteId(routeId);
            if (driverBusRoute1.getDriverId() == null&&driverBusRoute1.getBusId()==null) {
                //记录为空  未分配司机和汽车，现在分配汽车，直接插入表
                log.info("司机为空");
                driverBusRoute.setBusId(busId);
                driverBusRoute.setRouteId(routeId);
                driverBusRoute.setBusTime(new Date());
                driverBusRoute.setRouteStatus(0);
                driverBusRoute.setDriverStatus(0);
                driverBusRouteService.insert(driverBusRoute);
            } else if(driverBusRoute1.getBusId()!=null){
                return ResponseUtil.ok("不需要分配汽车");
            }
            else {

                //记录不为空  分配过司机，现在更新汽车信息，并且发送消息，更新汽车状态为占用
                log.info("不为空");
                driverBusRoute1.setBusId(busId);
                driverBusRoute1.setBusTime(new Date());
                driverBusRoute1.setRouteStatus(1);
                driverBusRouteService.updateDriverBusRoute(driverBusRoute1);
                //向购买线路的人发送消息
                List<PassengerRoute> passengerRouteList = passengerRouteService.findPassengerRouteList(route.getRouteId());
                if (passengerRouteList != null) {
                    Message message = new Message();
                    message.setSender("管理员");
                    message.setMark(0);
                    message.setSendtime(new Date());
                    message.setReceivetype("乘客");
                    message.setTitle("运行公告");
                    message.setContent("您申请的线路开始运行了了，请注意乘车信息，车牌号:" + busId + "!");
                    for (PassengerRoute passengerRoute : passengerRouteList) {
                        message.setReceiver(passengerRoute.getPassengerId());
                        messageService.insertMessage(message);
                    }
                }

            }
            //更改车辆状态为占用
            Bus bus=busService.findBusById(busId);
            bus.setBusStatus(2);
            busService.updatebus(bus);
        }
        return ResponseUtil.ok("分配成功");
    }

    @ResponseBody
    @PostMapping("/allotdriver")
    public Object allotDriver(@RequestBody String body) {
        Integer routeId = JacksonUtil.parseInteger(body, "routeId");
        String driverId = JacksonUtil.parseString(body, "driverId");

        if(routeId==null||driverId==null||driverId==""){
            log.info("线路或者司机不能为空");
        } else{
            DriverBusRoute driverBusRoute = new DriverBusRoute();

            //更改线路状态为运行中
            Route route = routeService.findRouteById(routeId);
            //查询是否有车辆，有就改变状态
            DriverBusRoute driverBusRoute1 = driverBusRouteService.findInfoByRouteId(routeId);
            log.info(driverBusRoute1.getRouteId());
            if (driverBusRoute1.getBusId()== null&&driverBusRoute1.getDriverId()==null) {

                //记录为空 未分配过司机和汽车 现在分配司机  直接插入表
                driverBusRoute.setRouteStatus(0);
                driverBusRoute.setDriverId(driverId);
                driverBusRoute.setRouteId(routeId);
                driverBusRoute.setDirverTime(new Date());
                driverBusRoute.setDriverStatus(0);
                driverBusRouteService.insert(driverBusRoute);


                //向司机发送消息 通知领取任务
                Message message = new Message();
                message.setSender("管理员");
                message.setMark(0);
                message.setSendtime(new Date());
                message.setReceivetype("司机");
                message.setTitle("任务公告");
                message.setContent("任务已经分配，请尽快领取车辆!");
                message.setReceiver(driverService.findDriverById(driverId).getDriverNum());
                messageService.insertMessage(message);
            } else if(driverBusRoute1.getDriverId()!=null) {
                return ResponseUtil.ok("不需要分配司机");
            }
            else {
                //向司机发送消息 通知领取任务
                Message message1 = new Message();
                message1.setSender("管理员");
                message1.setMark(0);
                message1.setSendtime(new Date());
                message1.setReceivetype("司机");
                message1.setTitle("任务公告");
                message1.setContent("任务已经分配，请尽快领取车辆!");
                message1.setReceiver(driverService.findDriverById(driverId).getDriverNum());
                messageService.insertMessage(message1);

                log.info("分配过汽车，现在分配司机");
                //分配过汽车  现在分配司机 同时更新运行中
                driverBusRoute1.setRouteStatus(1);
                driverBusRoute1.setDirverTime(new Date());
                driverBusRoute1.setDriverId(driverId);

                driverBusRouteService.updateDriverBusRoute(driverBusRoute1);

                //向购买线路的人发送消息
                List<PassengerRoute> passengerRouteList = passengerRouteService.findPassengerRouteList(route.getRouteId());
                if (passengerRouteList != null) {
                    Message message = new Message();
                    message.setSender("管理员");
                    message.setMark(0);
                    message.setSendtime(new Date());
                    message.setReceivetype("乘客");
                    message.setTitle("运行公告");
                    message.setContent("您申请的线路开始运行了了，请注意乘车信息，车牌号:" + driverBusRoute1.getBusId() + "!");
                    for (PassengerRoute passengerRoute : passengerRouteList) {
                        message.setReceiver(passengerRoute.getPassengerId());
                        messageService.insertMessage(message);
                    }
                }
            }
        }

        return ResponseUtil.ok();
    }

    @ResponseBody
    @PostMapping(value = "/setRouteStatus")
    /**
     *@type method
     *@parameter [body]
     *@back java.lang.Object
     *@author 如花
     *@creattime 2018/6/29
     *@describe 设置线路状态
     */
    public Object setRouteStatus(@RequestBody String body) {
        Integer routeId = JacksonUtil.parseInteger(body, "routeId");
        Integer routeStatus = JacksonUtil.parseInteger(body, "routeStatus");
        if (routeId == null || "".equals(routeId)) {
            return ResponseUtil.fail(500, "线路id不能为空");
        }
        Route route = routeService.findRouteById(routeId);
        if (route == null) {
            return ResponseUtil.fail(500, "未找到此线路");
        }
        if (routeStatus == null || "".equals(routeStatus)) {
            return ResponseUtil.fail(500, "线路状态不能为空");
        }
        route.setRouteStatus(routeStatus);
        routeService.updateRoute(route);
        return ResponseUtil.ok();
    }
}
