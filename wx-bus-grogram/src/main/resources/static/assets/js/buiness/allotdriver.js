$(document).ready(function () {
    //查找所有可用的司机
    var body={startNum:1,num:500};
    $.ajax({
        type: 'POST',
        url: '/web/search/searchcanusedriver',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var dropList = "";
            if(res.data.length<=0){
                dropList="<li class='selected'><a href='#'>无可用司机</a></li> ";
            }else{
                $(res.data).each(function (index, item) {
                    dropList += "<li class='selected' data-driverid='" + item.driverId +"'><a href='#'>司机姓名："+item.driverName+"&nbsp;&nbsp;司机账号:" + item.driverId + "</a></li>";
                })
            }
            $("ul.dropdown-menu.pull-right.station").append(dropList);

        }
    })

    //可用汽车的下拉框
    $(document).on("click","li.selected",function () {
        $("input[name='driverid']").val($(this).data("driverid"));

    })

    //提交更改

    $("button.btn.btn-info.btn-fill.btn-wd").click(function () {
        var data={};
        data.routeId=$("input[name='routeid']").val();
        data.driverId=$("input[name='driverid']").val();
        if(data.driverId==""){
            alert("请选择司机");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/web/update/allotdriver',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                alert("分配成功");
                $("button.btn.btn-info.btn-fill.btn-wd").attr("class","btn btn-info btn-fill btn-wd disabled")
                $("button.btn.btn-info.btn-fill.btn-wd").unbind();
            }
        })
    })

})