$(document).ready(function () {
    //没有更多数据
    var noneleft="<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing="<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";



    //初次加载 flag=0  发布招募的线路

    $('div.content.table-responsive.table-full-width').empty();
    var page=1;
    var body={startNum:page,num:10};
    $('input[type=hidden]').attr("value",page);

    $.ajax({
        type: 'POST',
        url: '/web/search/runcheckroute',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            if(res.data.length<=0){
                $('div.content.table-responsive.table-full-width').html(nothing);
            }else {
                var more="<div class='panel panel-success more-flag0'>"+
                    "<div class='panel-heading'>" +
                    "<center>" +
                    "<h3 class='panel-title'>点击加载更多数据</h3>" +
                    "</center>" +
                    "</div>" +
                    "</div>";
                var theader="<table class='table table-striped'>" +
                    "<thead>" +
                    "<th>线路编号</th>" +
                    "<th>创建人</th>" +
                    "<th>创建时间</th>" +
                    "<th>起点站</th>" +
                    "<th>终点站</th>" +
                    "<th>出发时间</th>" +
                    "<th>预计到达</th>" +
                    "<th>运行周期</th>" +
                    " <th>操作</th>"+
                    "</thead>" +
                    "<tbody></tbody>" +
                    "</table>";
                $('div.content.table-responsive.table-full-width').html(theader);
                var tbody="";
                var creatUser="";
                $(res.data).each(function (index,item) {
                    if(item.creatUser<=0){
                        creatUser="管理员";
                    }else{
                        creatUser=item.creatUser;
                    }
                    tbody+="<tr>"+
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "<td><a class='btn btn-primary' href='/web/details/updateline/"+item.routeId+"'>修改信息</a><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#myModal' data-id='"+item.routeId+"'>发布招募</button></td>"
                        "</tr>";
                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }else{
                    $('div.content.table-responsive.table-full-width').append(more);
                }
            }
        }

    });



    // 点击flag=0  发布招募的线路
    $('li.flag_0').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class","active flag_0");
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_2').attr("class","flag_2");
        $('li.flag_3').attr("class","flag_3");


        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);

        $.ajax({
            type: 'POST',
            url: '/web/search/runcheckroute',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag0'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        " <th>操作</th>"+
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    var tbody="";
                    var creatUser="";
                    $(res.data).each(function (index,item) {
                        if(item.creatUser<=0){
                            creatUser="管理员";
                        }else{
                            creatUser=item.creatUser;
                        }
                        tbody+="<tr>"+
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "<td><a class='btn btn-primary' href='/web/details/updateline/"+item.routeId+"'>修改信息</a><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#myModal' data-id='"+item.routeId+"'>发布招募</button></td>"+
                            "</tr>";
                    })
                    $('tbody').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        });
    });


    // flag=0  发布招募的线路,加载更多
    $(document).on("click","div.panel.panel-success.more-flag0",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runcheckroute',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                var creatUser="";
                $(res.data).each(function (index,item) {
                    if(item.creatUser<=0){
                        creatUser="管理员";
                    }else{
                        creatUser=item.creatUser;
                    }
                    tbody+="<tr>"+
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "<td><a class='btn btn-primary' href='/web/details/updateline/"+item.routeId+"'>修改信息</a><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#myModal' data-id='"+item.routeId+"'>发布招募</button></td>" +
                        "</tr>";

                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.panel.panel-success').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    });

    //--------------------------以下为招募列表线路-----------------------------

    //flag_1点击 点击招募列表
    $('li.flag_1').click(function () {
        $('div.content.table-responsive.table-full-width').empty();
        $('li.flag_1').attr("class","active flag_1");
        $('li.flag_0').attr("class","flag_0");
        $('li.flag_2').attr("class","flag_2");
        $('li.flag_3').attr("class","flag_3");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/recruiting',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag1'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "<th>招募结束日期</th>"+
                        "<th>招募进度</th>"+
                        "<th>操作</th>"+
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    var creatUser="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        if(item.creatUser<=0){
                            creatUser="管理员";
                        }else{
                            creatUser=item.creatUser;
                        }
                        tbody+="<tr id='"+item.routeId+"'>" +
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "<td>"+item.endsRecruit+"</td>" +
                            "<td>" +
                            "<div class='progress'>" +
                            "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: "+item.recruitNum+"%;'>" +
                            "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                            "</div>" +
                            "</div>" +
                            "</td>" +
                            "<td><button class='btn examrecruit' data-status='5' data-rid='"+item.routeId+"'>完成招募</button><button class='btn examrecruit' data-status='4' data-rid='"+item.routeId+"'>取消招募</button></td>" +
                            "</tr>";
                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        })
    });
    //flag1  招募列表加载更多
    $(document).on("click","div.panel.panel-success.more-flag1",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/recruiting',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                var creatUser="";
                $(res.data).each(function (index, item) {
                    if(item.creatUser<=0){
                        creatUser="管理员";
                    }else{
                        creatUser=item.creatUser;
                    }
                    tbody+="<tr "+item.routeId+">" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "<td>"+item.endsRecruit+"</td>" +
                        "<td>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width:"+item.recruitNum+"%;'>" +
                        "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                        "</div>" +
                        "</div>" +
                        "</td>" +
                        "<td><button class='btn examrecruit' data-rid='"+item.routeId+"' data-status='5'>完成招募</button><button class='btn examrecruit' data-rid='"+item.routeId+"' data-status='4'>取消招募</button></td>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.panel.panel-success').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }
        });

    })
//---------------------------------------以下是分配汽车--------------------------------------
    //flag_2点击 分配汽车
    $('li.flag_2').click(function () {
        $('div.content.table-responsive.table-full-width').empty();
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_0').attr("class","flag_0");
        $('li.flag_2').attr("class","active flag_2");
        $('li.flag_3').attr("class","flag_3");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroute/allotbus',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag2'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "<th>招募结束日期</th>"+
                        "<th>招募进度</th>"+
                        "<th>操作</th>"+
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    var creatUser="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        if(item.creatUser<=0){
                            creatUser="管理员";
                        }else{
                            creatUser=item.creatUser;
                        }
                        tbody+="<tr>" +
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "<td>"+item.endsRecruit+"</td>" +
                            "<td>" +
                            "<div class='progress'>" +
                            "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: "+item.recruitNum+"%;'>" +
                            "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                            "</div>" +
                            "</div>" +
                            "</td>" +
                            "<td><a class='btn btn-primary' href='/web/details/allotbus/"+item.routeId+"'>分配汽车</a>" +
                            "</tr>";
                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        })


    });

    //flag2  分配汽车加载更多
    $(document).on("click","div.panel.panel-success.more-flag2",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroute/allotbus',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                var creatUser="";
                $(res.data).each(function (index, item) {
                    if(item.creatUser<=0){
                        creatUser="管理员";
                    }else{
                        creatUser=item.creatUser;
                    }
                    tbody+="<tr>" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "<td>"+item.endsRecruit+"</td>" +
                        "<td>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: "+item.recruitNum+"%;'>" +
                        "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                        "</div>" +
                        "</div>" +
                        "</td>" +
                        "<td><a class='btn btn-primary' href='/web/details/allotbus/"+item.routeId+"'>分配汽车</a>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.panel.panel-success').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }
        });

    })
    /**
     * 分配司机
     *
     */
    //flag_2点击 分配司机
    $('li.flag_3').click(function () {
        $('div.content.table-responsive.table-full-width').empty();
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_0').attr("class","flag_0");
        $('li.flag_2').attr("class","flag_2");
        $('li.flag_3').attr("class","active flag_3");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroute/allotdriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag3'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "<th>招募结束日期</th>"+
                        "<th>招募进度</th>"+
                        "<th>操作</th>"+
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    var creatUser="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        if(item.creatUser<=0){
                            creatUser="管理员";
                        }else{
                            creatUser=item.creatUser;
                        }
                        tbody+="<tr>" +
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "<td>"+item.endsRecruit+"</td>" +
                            "<td>" +
                            "<div class='progress'>" +
                            "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: "+item.recruitNum+"%;'>" +
                            "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                            "</div>" +
                            "</div>" +
                            "</td>" +
                            "<td><a class='btn btn-primary' href='/web/details/allotdriver/"+item.routeId+"'>分配司机</a>" +
                            "</tr>";
                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else{
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        })


    });

    //flag2  分配司机加载更多
    $(document).on("click","div.panel.panel-success.more-flag3",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroute/allotdriver',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                var creatUser="";
                $(res.data).each(function (index, item) {
                    if(item.creatUser<=0){
                        creatUser="管理员";
                    }else{
                        creatUser=item.creatUser;
                    }
                    tbody+="<tr>" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "<td>"+item.endsRecruit+"</td>" +
                        "<td>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: "+item.recruitNum+"%;'>" +
                        "<span class='sr-only'>"+item.recruitNum+"% 完成</span>" +
                        "</div>" +
                        "</div>" +
                        "</td>" +
                        "<td><a class='btn btn-primary' href='/web/details/allotdriver/"+item.routeId+"'>分配司机</a>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.panel.panel-success').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }
        });

    })
    /**
     * 修改线路状态
     */
    $(document).on("click","button.examrecruit",function () {
        var Route = {};
        Route.routeId = $(this).data("rid");
        Route.routeStatus = $(this).data("status");

        $.ajax({
            url: '/web/update/setRouteStatus',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(Route),
            error: function () {
                alert("加载失败，请刷新重试！");
            },
            success: function (res) {
                $('tr#' + Route.routeId).remove();
            }
        })


    })
    /**
     * 填写招募信息
     */

    $(document).on("click","button[data-target='#myModal']",function () {
        $("input[name='routId']").val($(this).data("id"));
        $(document).on("click","button.btn.btn-primary.submit",function () {
            var Route={};
            Route.routeId= $("input[name='routId']").val();

            Route.recruitNum= $("input[name='recruitNum']").val();
            if(Route.recruitNum==""){
                alert("请输入招募人数");
                return false;
            }
            Route.price= $("input[name='price']").val();
            if(Route.price==""){
                alert("请输入票价");
                return false;
            }
            $.ajax({
                url:'/web/update/updateroute',
                type:'POST',
                dataType:'json',
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(Route),
                error:function () {
                    alert("加载失败，请刷新重试！");
                },
                success:function (res) {
                    if(res.errno==0){
                        window.location.reload();
                    }
                }
            })
        })

    })
})